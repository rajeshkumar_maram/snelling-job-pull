package com.imomentous;


import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


public class SendEmailSSL {
    //Secure Sockets Layer (SSL).


    public static void sendEmail(String subject, String content) {

//        readProperties();
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("do-not-reply@imomentous.com", "12345678");
                    }
                });

        try
        {
            /*Address[] addresses=new Address[5];
			addresses[0]=InternetAddress.parse("rajeshkumar.maram@imomentous.com");*/

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("do-not-reply@imomentous.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("rajeshkumar.maram@imomentous.com"));//emailsList
            message.setSubject(subject);
            message.setText("Hi ," + "\n\n status Message: " + content
                    + "\n\n-------------------------------------");

            Transport.send(message);

            System.out.println(" Email sent ");


        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        sendEmail("hi this is snelling test mail ", "bye Testing over");
    }
}