package com.imomentous;

import com.mysql.jdbc.Statement;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.xml.sax.InputSource;

/**
 * Created by rajesh on 22/7/14.
 */
public class SnellingJobPullXMLFeed
{
    static PrintWriter pw = null;
    static String jid=null,jtitle=null,jloc=null,jtype=null,jindustry=null,jpayrate=null,jpostdate=null,country=null,state=null,city=null,jdepartment=null,jcategory=null;
    static String pgsrc=null,jpay=null;
    static String jd=null,jdteaser=null;
    static String pubrefnum=null,jzipcode=null,jurl=null,jemail=null;
    static int did=0,k=1;
    static HashMap< String, String> hm=new HashMap< String, String>();
    public static final Logger log = Logger.getLogger("SnellingJobPullXMLFeed.class.getName()");
    static java.sql.Date sqldate1=null,sqldate2=null;
    static Connection con = null;
    static HashMap<String, Integer> mapMonthsNumbers=new HashMap<String,Integer>();

    static String reportEmail=null;
    static String sendStartMail=null;
    static boolean isBotSuccess=true;

    static String newXmlField=null;

    public static String getCurrentTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    static
    {
        mapMonthsNumbers.put("January",1);
        mapMonthsNumbers.put("February",2 );
        mapMonthsNumbers.put("March", 3);
        mapMonthsNumbers.put("April",4 );
        mapMonthsNumbers.put("May", 5);
        mapMonthsNumbers.put("June",6 );
        mapMonthsNumbers.put("July", 7);
        mapMonthsNumbers.put("August",8 );
        mapMonthsNumbers.put("September",9 );
        mapMonthsNumbers.put("October", 10);
        mapMonthsNumbers.put("November", 11);
        mapMonthsNumbers.put("December", 12);
    }

    public static Connection getConnection()  throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Connection conn = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "jobpulls";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "root";

        Class.forName(driver).newInstance();
        conn = DriverManager.getConnection(url + dbName, userName, password);

        return conn;
    }


    public static Connection getConnection1()  throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Connection conn = null;
        String url = "jdbc:mysql://aa.chuovgihlxvf.us-east-1.rds.amazonaws.com:3306/";
        String dbName = "jobpulls";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "imomentous";
        String password = "imomentous";

        Class.forName(driver).newInstance();
        conn = DriverManager.getConnection(url+dbName,userName,password);
        return conn;
    }

    public static String getId(String locationData)
    {
        String zipCode=null;
        try
        {
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(locationData);
            if (m.find())
            {
                //System.out.println("only numbers: "+m.group());
                zipCode=m.group();
                //System.out.println("found zip code as: "+zipCode);
                return zipCode;
            }
        }
        catch(Exception e)
        {
            System.out.println("cant retrieve zipcode exception:");
        }
        return zipCode;

    }

    private static String readFromFile(File fileLocation) throws IOException
    {
       /* String str  = "𠀀"; //U+20000, represented by 2 chars in java (UTF-16 surrogate pair)
        str = str.replaceAll( "([\\ud800-\\udbff\\udc00-\\udfff])", "");
        System.out.println(str.length()); //0*/
        StringBuffer sb = new StringBuffer("UTF8");
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileLocation),"UTF8"));
        String s;
        System.out.println("entering into reading file");
        while ((s = br.readLine()) != null)
        {
            //  s = s.replaceAll( "([\\ud800-\\udbff\\udc00-\\udfff])", " ");
            sb.append(s);
            //sb.append("\n");
        }
        br.close();

       /* String temp= StringEscapeUtils.unescapeXml(sb.toString());
        String temp1= StringEscapeUtils.unescapeXml(temp);*/
        String temp=StringEscapeUtils.unescapeHtml(sb.toString());
        String temp1=StringEscapeUtils.unescapeHtml(temp);
        return temp1;
        //
    }


    public static void snellingJobsXMLFeed(File path) throws Exception
    //public static void snellingJobsXMLFeed(String path) throws Exception
    {

        try
        {
//            String path="/home/rajesh/Desktop/temp/xmlfeeds/snelling.xml";

            //File file = new File("config/saxtest.xml");
            InputStream inputStream= new FileInputStream(path);
            Reader reader = new InputStreamReader(inputStream,"UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");


            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dBuilder.parse(path);
            Document doc = dBuilder.parse(is);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("job");
            System.out.println("length : "+nList.getLength());
            String company=null;

            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                jtitle=null;
                jid=null;
                jloc=null;
                jpay=null;jtype=null;
                jpostdate=null;sqldate1=null;
                jd=null; jdteaser=null;jindustry=null; jurl=null;
                jcategory=null;city=null;state=null; jemail=null;jzipcode=null;country=null;
                company=null;
                newXmlField=null;


                Node nNode = nList.item(temp);

                // System.out.println("\nCurrent Element :" + nNode.getNodeName());
                ++did;
                if (nNode.getNodeType() == Node.ELEMENT_NODE)
                {

                    Element eElement = (Element) nNode;
                    //    System.out.println("id : "+temp);
//                    id = temp;

                    jtitle = eElement.getElementsByTagName("title").item(0).getTextContent().trim();
                    jid = eElement.getElementsByTagName("referencenumber").item(0).getTextContent().trim();
                    jurl = eElement.getElementsByTagName("url").item(0).getTextContent().trim();
                    company = eElement.getElementsByTagName("company").item(0).getTextContent().trim();
                    city = eElement.getElementsByTagName("city").item(0).getTextContent().trim();
                    state = eElement.getElementsByTagName("state").item(0).getTextContent().trim();
                    country = eElement.getElementsByTagName("country").item(0).getTextContent().trim();
                    jzipcode = eElement.getElementsByTagName("postalcode").item(0).getTextContent().trim();
                    jd = eElement.getElementsByTagName("description").item(0).getTextContent().trim();
                    jpostdate = eElement.getElementsByTagName("date").item(0).getTextContent().trim();
                    jemail=eElement.getElementsByTagName("contactemail").item(0).getTextContent().trim();
                    jcategory=eElement.getElementsByTagName("category").item(0).getTextContent().trim();
                    jtype=eElement.getElementsByTagName("jobtype").item(0).getTextContent().trim();
                    newXmlField=eElement.getElementsByTagName("EntakeAppUrlJobID").item(0).getTextContent().trim();

                    sqldate1=getSqlDate(jpostdate,jurl);

                    jdteaser=jd.replaceAll("\\<.*?>","").trim().replaceAll("\\s+"," ");

                    if(newXmlField.contains("application"))
                        jd+="<br/><span class=\"entakeappurl\">Please <a href="+newXmlField+">click here</a> to complete the application.<span>";



                    //  String posteddate =   getCurrentDate(date);

                    jloc =city+", "+state +", "+country;//+", "+jzipcode;

//                    curDate = getCurrentDate1();
//                    storeData(id, city, company, curDate, desc, jobId, location, ref_num, title, url, state, industry,department,industry,zipcode,curDate,category);

                    try
                    {
                        if(jurl!=null)
                        {
                            String language_type="en";
                            String ref_num=pubrefnum;
                            String job_seq_no=pubrefnum+did;
                            String curDate = getCurrentTime();
                            storeData(did,jtitle,jurl,jid,jd,country,jcategory,jloc,state,city,jtype,company,curDate,language_type,ref_num,job_seq_no,sqldate1,jdepartment,jindustry,jzipcode,jid,jemail,jpostdate,jdteaser);
                            System.out.println("Job saved with seqNO:" + job_seq_no);
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println("Storing Exception found: "+e.getMessage());
                        e.printStackTrace();
                    }
                }
            }

            if(path.delete())
                System.out.println("file successfully removed");
            else
                System.out.println("problem with deletion");
        }
        catch (Exception e)
        {
            isBotSuccess=false;
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            String errorMessage=errors.toString();
            SendEmailSSL.sendEmail("Snelling XML scrape issue(job wise):", " Error Message as:  \n " + errorMessage + " and \n gist of the Exception as: " + e.getMessage() +"\n failed for url: \n ");
            e.printStackTrace();
        }
    }

    public static void storeData(int j,String jtitle,String jUrl,String jid,String wholejobcontent,String country,String category,String loc,String state,String city,String jtype,String company_name,String curDate,String language_type,String ref_num,String job_seq_no,java.sql.Date postdate,String department,String jindustry,String jzipcode,String cms,String jemail,String jpostdate,String jdteser)
    {
        PreparedStatement prep=null;
        //Connection con = getConnection();

        try
        {
            // System.out.println("cms id formed as: "+cms);
            prep = con.prepareStatement( "INSERT into jobpulls.snellingjobs(category,city,company_name,date,job_description,job_id,job_location,job_seq_no,job_title,job_url,language_type,ref_num,state,id,job_type,post_date,reserved3,department,industry,zip_code,cms_job_id,mail_to,job_description_teaser) values (?, ?,?,?, ?,?, ?,?, ?,?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");


            prep.setString(1,category);
            prep.setString(2,city);
            prep.setString(3,company_name);
            prep.setString(4,curDate);
            prep.setString(5,wholejobcontent);
            prep.setString(6,jid);
            prep.setString(7,loc);
            prep.setString(8,job_seq_no);
            prep.setString(9,jtitle);
            prep.setString(10,jUrl);
            prep.setString(11,language_type);
            prep.setString(12,ref_num);
            prep.setString(13,state);
            prep.setInt(14,j);
            prep.setString(15,jtype);
            prep.setDate(16, postdate);
//            prep.setString(16,jpostdate);
            prep.setString(17,country);
            prep.setString(18,department);
            prep.setString(19,jindustry);
            prep.setString(20,jzipcode);
            prep.setString(21,cms);
            prep.setString(22,jemail);
            prep.setString(23,jdteser);
//            prep.setString(23,jpostdate);


            prep.executeUpdate();
            //System.out.println("job stored into CC:"+j);
            //System.out.println("JD 3");
        }
        catch (SQLException e)
        {
            isBotSuccess=false;
            SendEmailSSL.sendEmail("store exception under snelling job storage:",e.getMessage());
            e.printStackTrace();
        }
        //System.out.println("JD 2");


    }

    public static java.sql.Date getSqlDate(String jpostdate,String jurl)
    {
        try
        {
            java.sql.Date sqldate=null;
            String month=null;

            String[] dateDetails=jpostdate.split(" ");

            if(dateDetails.length>=2)
            {
                String partialMmonth=jpostdate.split(" ")[0];
                for(String mth:mapMonthsNumbers.keySet())
                {
                    if(mth.toLowerCase().contains(partialMmonth.toLowerCase()))
                        month=mapMonthsNumbers.get(mth).toString();
                }

                if(month.length()==1)
                {
                    month=0+month;
                }
                String day=dateDetails[1];
                String year=dateDetails[2];
//                System.out.println("found day only as:"+day + " and length as: "+day.length());
                if(day.length()==0)//day==" "
                {
                    day=dateDetails[2];
                    year=dateDetails[3];
                }
                if(day.length()==1)
                {
                    day=0+day;
                }
                year=year.replaceAll("\\s","");
                SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");//yyyy-MM-dd
//                                Date parsed = format.parse("07-15-2013");//February 17, 2014
                Date parsed = format.parse(month+"-"+day+"-"+year);
                sqldate = new java.sql.Date(parsed.getTime());
            }
            if(sqldate!=null)
            {
                return sqldate;
            }
        }


        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("problem with post date: "+jpostdate);
            System.out.println("url found : "+jurl);
//            System.exit(-1);
            return  null;
        }

        return null;
    }

    private static File getFirstFile(String path)
    {
        File f = null;
        File[] paths;

        try
        {
            // create new file
            f = new File(path);

            // returns pathnames for files and directory
            paths = f.listFiles();
            System.out.println("total files found: "+paths.length);
            if(paths.length==0)
            {
                SendEmailSSL.sendEmail("no snelling feed found: "," 0 files found in xml feed directory \n job scrape stopped");
                System.exit(0);
            }

            // for each pathname in pathname array
            for(File filepath:paths)
            {
                System.out.println("first file found : "+filepath);
                return filepath;
            }
        }catch(Exception e){
            // if any error occurs
            e.printStackTrace();
        }
        return null;
    }

    public static boolean controlUpdate(String tableName,String refNum,String status)
    {
        try
        {
            Statement statement = (Statement) con.createStatement();
            String sql="update "+tableName +" SET status='"+status+"'" + " WHERE refnum='"+refNum+"'";
            statement.executeUpdate(sql);
            System.out.println(" tenant "+tableName +" status updated to "+status +" successfully");
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }



    public static void main(String[] args) throws MalformedURLException, InterruptedException
    {
        pubrefnum="SNELA001N";//prod-SNELA001N,qa-SNELA002O
        String fp=null;
         Properties prop = null;
      try {
          prop = new Properties();
          prop.load(new FileInputStream("config/robot.properties"));
          //	prop.load(new FileInputStream("/home/manjunath/apply_robots/QvcJobPullRobot/config/robot.properties"));
//          pubrefnum = prop.getProperty("refnum");
          fp=prop.getProperty("xmlfeed");
          System.out.println("found: "+fp);
          pubrefnum = prop.getProperty("refnum");
          reportEmail= prop.getProperty("email");
          sendStartMail=  prop.getProperty("sendStartMail");

      }
      catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      }

       /* getFirstFile(fp);

        System.exit(0);*/

        try
        {
            con=getConnection1();

            System.out.println("Snelling jobs update is going to be Locked:");
            controlUpdate("tenant_status",pubrefnum,"no");

            if(sendStartMail.equals("true"))
            {
                System.out.println("start mail is set to true");
                SendEmailSSL.sendEmail("Snelling jobs scrape is started: ","scrape is about to start");
            }
            else
            {
                System.out.println("start mail is set to false");
            }
            Statement st = (Statement) con.createStatement();

            st.executeUpdate("DELETE FROM jobpulls.snellingjobs WHERE id>=0");
            System.out.println("Database cleared:");
            System.out.println("Inserting new jobs");

            File fileName=getFirstFile(fp);

            System.out.println("caught here: "+fileName);

//           snellingJobsXMLFeed(fileName);
            //String xmlContent=readFromFile(fileName);
            /*System.out.println("xml content:"+xmlContent);
            System.out.println("**********************************************************************************************");
            System.exit(0);*/
           snellingJobsXMLFeed(fileName);

            /*if(fileName.delete())
                System.out.println("file successfully removed");
            else
                System.out.println("problem with deletion");*/

            if(isBotSuccess)
            {
                controlUpdate("tenant_status",pubrefnum,"yes");
                System.out.println("Snelling jobs update is Unlocked:");
            }

            if(con!=null)
                con.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
