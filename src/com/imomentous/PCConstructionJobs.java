package com.imomentous;

import com.mysql.jdbc.Statement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rajesh on 2/6/14.
 */
public class PCConstructionJobs
{

    static WebDriver driver=null;

    //static WebDriver driver1=null;

    static PrintWriter pw = null;


    static List<String> allDepartments=new ArrayList<String>();
    static List<String> allTitles=new ArrayList<String>();
    static List<String> allLocations=new ArrayList<String>();


    static List<String> jblinks=new ArrayList<String>();
    static List<String> jcategories=new ArrayList<String>();
    static String jid=null,jtitle=null,jloc=null,jtype=null,jindustry=null,jpayrate=null,jpostdate=null,country=null,state=null,city=null,jdepartment=null,jcategory=null;
    static String pgsrc=null,jpay=null;
    static String jd=null,jdteaser=null,cmsJobId=null;
    static String pubrefnum=null,jzipcode=null,jurl=null;
    static int did=0,k=1;
    static HashMap< String, String> hm=new HashMap< String, String>();
    static DesiredCapabilities capability = DesiredCapabilities.firefox();
    public static final Logger log = Logger.getLogger("SnellingJobPull.class.getName()");
    static java.sql.Date sqldate1=null,sqldate2=null;
    static Connection con = null;


    public static String getCurrentDate()
    {
        String retDate="";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        retDate = sdf.format(date);
        if(retDate!=null){
            return retDate;
        }else{
            return  null;
        }

    }
    public static String getCurrentTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static Connection getConnection()  throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Connection conn = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "jobpulls";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "root";

        Class.forName(driver).newInstance();
        conn = DriverManager.getConnection(url + dbName, userName, password);

        return conn;
    }


    public static Connection getConnection1()  throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Connection conn = null;
        String url = "jdbc:mysql://aa.chuovgihlxvf.us-east-1.rds.amazonaws.com:3306/";
        String dbName = "jobpulls";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "imomentous";
        String password = "imomentous";

        Class.forName(driver).newInstance();
        conn = DriverManager.getConnection(url+dbName,userName,password);
        return conn;
    }

    public static String getId(String locationData)
    {
        String id=null;
        try
        {
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(locationData);
            if (m.find())
            {
                //System.out.println("only numbers: "+m.group());
                id=m.group();
                return id;
            }
        }
        catch(Exception e)
        {
            System.out.println("cant retrieve zipcode exception:");
        }
        return id;

    }
    public static String getSecondId(String locationData)
    {
        String id=null;
        try
        {
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(locationData);
            int it=0;
            while(m.find())
            {
                ++it;
                //System.out.println("only numbers: "+m.group());
                if(it==2)
                {
                    id=m.group();
                    //System.out.println("found code as: "+zipCode);
                    return id;
                }

            }
        }
        catch(Exception e)
        {
            System.out.println("cant retrieve zipcode exception:");
        }
        return id;

    }

    public static void pcConstructionJobs()
    {
        driver.get("http://www.hirebridge.com/v3/jobs/list.aspx?cid=5835");
        List<WebElement> joburls=driver.findElements(By.className("job"));
        List<WebElement> departments=driver.findElements(By.className("department"));
        System.out.println("total jobs count : "+joburls.size() + " and total departments: "+departments.size());

        for (int i=0;i<joburls.size();++i)
        {
            System.out.println("processing i value: "+i);
            allTitles.add(joburls.get(i).getText());
            jblinks.add(joburls.get(i).findElement(By.tagName("a")).getAttribute("href"));
            allDepartments.add(departments.get(i).getText());
        }

        System.out.println("all titles: "+allTitles.toString());
        System.out.println("all job urls: "+jblinks.toString());
        System.out.println("all departments: "+allDepartments.toString());

        System.out.println("actual total jobs count : "+jblinks.size() + " and total departments: "+allDepartments.size());

        driver.get("http://www.hirebridge.com/v3/Jobs/JobDetails.aspx?cid=5835&jid=221127");



        for (int i=0;i<jblinks.size();++i)
        {
            try
            {
                ++did;
                jtitle=null;
                jid=null;
                jloc=null;
                jpay=null;jtype=null;
                jpostdate=null;sqldate1=null;
                jd=null; jdteaser=null;jindustry=null; jurl=null;cmsJobId=null;
                jcategory=null;city=null;state=null;jdepartment=null;
                jurl=jblinks.get(i);

                driver.get(jurl);


                WebElement we3=driver.findElement(By.id("ctl00_pageContent_ctl00_html_jobdesc"));
                jd = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", we3);
                jdteaser=we3.getText();
                jtitle=driver.findElement(By.id("ctl00_pageContent_ctl00_jobtitle")).getText();
                jloc=driver.findElement(By.id("ctl00_pageContent_ctl00_jobloc")).getText();
                if(jloc.contains(","))
                    jloc=jloc.replaceAll("\\s","");
                jdepartment=allDepartments.get(i);
                jcategory=jdepartment;

                cmsJobId=jurl.substring(jurl.indexOf("=")+1,jurl.indexOf("&"));
                jid=jurl.substring(jurl.lastIndexOf("=")+1);

                try
                {
                    String loc[]=jloc.split(",");
                    if(loc.length==2)
                    {
                        city=loc[0];
                        state=loc[1];
                    }
                    else
                    {
                        state=jloc;
                    }
                }
                catch(Exception e)
                {
                    System.out.println("location exception found");
                    e.printStackTrace();
                }

                System.out.println("------------------------------------------------------------------------------------------------------------");
                System.out.println("found job description : "+jd);
                System.out.println("found job description teaser: "+jdteaser);
                System.out.println("found job title: "+jtitle);
                System.out.println( "found job location : "+jloc);
                System.out.println("city and state as :"+city +" "+state);
                System.out.println("cms job id as: "+cmsJobId);
                System.out.println("job id as: "+jid);
                System.out.println("job department as: "+jdepartment);
                System.out.println("job url as: "+jurl);
                System.out.println("------------------------------------------------------------------------------------------------------------");

                try
                {
                    if(jurl!=null)
                    {
                        String language_type="en";
                        //String ref_num="INFOA000A";
                        String ref_num=pubrefnum;
                        //String job_seq_no="INFOA000A"+did;
                        String job_seq_no=pubrefnum+did;
                        String curDate = getCurrentTime();
                        //jtitle=hm.get(ur);
                        // jid=job_seq_no;
                        storeData(did,jtitle,jurl,jid,jd,jdteaser,jcategory,jloc,state,city,cmsJobId,"PC Construction",curDate,language_type,ref_num,job_seq_no,sqldate1,jdepartment);
                        System.out.println("Job saved with seqNO:"+job_seq_no);
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Storing Exception found: "+e.getMessage());
                    e.printStackTrace();
                }

               // System.exit(0);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        driver.quit();





    }

    public static void storeData(int j,String jtitle,String jUrl,String jid,String wholejobcontent,String wholejobcontent1,String category,String loc,String state,String city,String cmsJobId,String company_name,String curDate,String language_type,String ref_num,String job_seq_no,java.sql.Date postdate,String department) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException
    {
        PreparedStatement prep=null;
        //Connection con = getConnection();

        try
        {


            // System.out.println("cms id formed as: "+cms);
            prep = con.prepareStatement( "INSERT into jobpulls.pcconstructionjobs(category,city,company_name,date,job_description,job_id,job_location,job_seq_no,job_title,job_url,language_type,ref_num,state,id,cms_job_id,post_date,job_description_teaser,department) values (?, ?,?,?, ?,?, ?,?, ?,?, ?,?, ?, ?, ?, ?, ?, ?)");


            prep.setString(1,category);
            prep.setString(2,city);
            prep.setString(3,company_name);
            prep.setString(4,curDate);
            prep.setString(5,wholejobcontent);
            prep.setString(6,jid);
            prep.setString(7,loc);
            prep.setString(8,job_seq_no);
            prep.setString(9,jtitle);
            prep.setString(10,jUrl);
            prep.setString(11,language_type);
            prep.setString(12,ref_num);
            prep.setString(13,state);
            prep.setInt(14,j);
            prep.setString(15,cmsJobId);
//			 prep.setDate(16, null);
            prep.setDate(16, postdate);
            prep.setString(17,wholejobcontent1);
            prep.setString(18,department);
            prep.executeUpdate();
            //System.out.println("job stored into CC:"+j);
            //System.out.println("JD 3");
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println("JD 2");


    }

    public static void main(String[] args) throws MalformedURLException, InterruptedException
    {
        pubrefnum="PCCOA002Q";
        //snellingjobs
        driver =new RemoteWebDriver(new URL("http://localhost:9515"), DesiredCapabilities.chrome());
         /*Properties prop = null;
      try {
          prop = new Properties();
          prop.load(new FileInputStream("/home/ubuntu/JobPull/informatica_new/config/robot.properties"));
          //	prop.load(new FileInputStream("/home/manjunath/apply_robots/QvcJobPullRobot/config/robot.properties"));

          refnum = prop.getProperty("refnum");
          pubrefnum = prop.getProperty("refnum");

      } catch (FileNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      }*/

        try
        {

            con=getConnection();
            Statement st = (Statement) con.createStatement();

            st.executeUpdate("DELETE FROM jobpulls.pcconstructionjobs WHERE id>0");
            System.out.println("Database cleared:");
            System.out.println("Inserting new jobs");

            pcConstructionJobs();

            if(con!=null)
            {
                con.close();
                System.out.println("connection closed");
            }

            if(driver!=null)
                driver.quit();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

}
