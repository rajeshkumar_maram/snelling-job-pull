package com.imomentous;

import com.mysql.jdbc.Statement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *  Created by rajesh on 2/6/14.
 */
public class SnellingJobPull
{
    static WebDriver driver=null;

    //static WebDriver driver1=null;

    static PrintWriter pw = null;

    static List<String> allDates=new ArrayList<String>();
    static List<String> allTitles=new ArrayList<String>();
    static List<String> allLocations=new ArrayList<String>();


    static List<String> jblinks=new ArrayList<String>();
    static List<String> jcategories=new ArrayList<String>();
    static String jid=null,jtitle=null,jloc=null,jtype=null,jindustry=null,jpayrate=null,jpostdate=null,country=null,state=null,city=null,jdepartment=null,jcategory=null;
    static String pgsrc=null,jpay=null;
    static String jd=null,jdteaser=null;
    static String pubrefnum=null,jzipcode=null,jurl=null,jemail=null;
    static int did=0,k=1;
    static HashMap< String, String> hm=new HashMap< String, String>();
    static DesiredCapabilities capability = DesiredCapabilities.firefox();
    public static final Logger log = Logger.getLogger("SnellingJobPull.class.getName()");
    static java.sql.Date sqldate1=null,sqldate2=null;
    static  Connection con = null;


	/*
	using selenium to get inner html of a tag (which is found using xpath or class By )

	 WebElement we1=driver.findElement(By.xpath("/html/body/div[3]/table/tbody/tr[7]"));
	String descContent = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", we1);
	 */




    public static String getCurrentDate()
    {
        String retDate="";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        retDate = sdf.format(date);
        if(retDate!=null){
            return retDate;
        }else{
            return  null;
        }

    }
    public static String getCurrentTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static Connection getConnection()  throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Connection conn = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "jobpulls";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "root";

        Class.forName(driver).newInstance();
        conn = DriverManager.getConnection(url + dbName, userName, password);

        return conn;
    }


    public static Connection getConnection1()  throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Connection conn = null;
        String url = "jdbc:mysql://aa.chuovgihlxvf.us-east-1.rds.amazonaws.com:3306/";
        String dbName = "jobpulls";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "imomentous";
        String password = "imomentous";

        Class.forName(driver).newInstance();
        conn = DriverManager.getConnection(url+dbName,userName,password);
        return conn;
    }

    public static String getId(String locationData)
    {
        String zipCode=null;
        try
        {
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(locationData);
            if (m.find())
            {
                //System.out.println("only numbers: "+m.group());
                zipCode=m.group();
                //System.out.println("found zip code as: "+zipCode);
                return zipCode;
            }
        }
        catch(Exception e)
        {
            System.out.println("cant retrieve zipcode exception:");
        }
        return zipCode;

    }

    public static void snellingJobs() throws InterruptedException
    {
        driver.get("http://www.snelling.com/JobSearch.aspx");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_btnSearch_0")).click();
        Select select = new Select(driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_ddlPageSize_0")));
        select.selectByVisibleText("All");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(5000);
        List<WebElement> dates=driver.findElements(By.className("left-column"));
        List<WebElement> titles=driver.findElements(By.className("middle-column"));
        List<WebElement> locations=driver.findElements(By.className("right-column"));

       /* System.out.println("first date as :"+dates.get(1).getText());
        System.out.println("first title as :"+titles.get(1).getText());
        System.out.println("first hyper link as: " + titles.get(1).findElement(By.tagName("a")).getAttribute("href"));
        System.out.println("first location as :"+locations.get(1).getText());*/

        System.out.println("total no of dates ,titles, and locations as: "+dates.size() +"and "+titles.size() +" and "+locations.size());

        for (int i=1;i<dates.size();++i)
        {
            System.out.println("processing i value: "+i);
            allDates.add(dates.get(i).getText());
//            allTitles.add(titles.get(i).getText());
            jblinks.add(titles.get(i).findElement(By.tagName("a")).getAttribute("href"));
//            allLocations.add(locations.get(i).getText());
        }
       // int i=0;
       // System.out.println("Actual total no of dates ,titles, and locations as: "+allDates.size() +"and "+allTitles.size() +" and "+allLocations.size());

            //for(WebElement titleLink:titles)
            //for (int i=1;i<dates.size();++i)
        for (int i=0;i<jblinks.size();++i)
            {
            try
                {
                   // ++i;
                    ++did;
                    jtitle=null;
                    jid=null;
                    jloc=null;
                    jpay=null;jtype=null;
                    jpostdate=null;sqldate1=null;
                    jd=null; jdteaser=null;jindustry=null; jurl=null;
                    jcategory=null;city=null;state=null; jemail=null;
                    //jurl=titles.get(i).findElement(By.tagName("a")).getAttribute("href");
                    jurl=jblinks.get(i);

                    driver.get(jurl);
                    WebElement we3=driver.findElement(By.className("description"));
                    jd = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", we3);
                    jdteaser=we3.getText();

                    jloc=driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_lblJobLocation_0")).getText();
                    jpay=driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_lblJobPay_0")).getText();
                    jtype=driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_lblJobType_0")).getText();
                    jindustry=driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_lblJobIndustry_0")).getText();
                    jtitle=driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_lblJobTitle_0")).getText();
                    jemail=driver.findElement(By.id("Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_lblOfficeEmail_0")).getText();
                    //Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_lblOfficeEmail_0

                    jzipcode=getId(jloc);
                    jid=getId(jurl);
                    jloc=jloc.replace(jzipcode,"");
                    jloc=jloc.replaceAll("\\s","");
                    jcategory=jindustry;
                    //jpostdate=dates.get(i).getText();
                    //jtitle=titles.get(i).getText();
                    jpostdate=allDates.get(i);
                    //jtitle=allTitles.get(i);
                    try
                    {
                        String loc[]=jloc.split(",");
                        if(loc.length==2)
                        {
                            city=loc[0];
                            state=loc[1];
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println("location exception found");
                        e.printStackTrace();
                    }

                    try
                    {
                        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
                        Date parsed=null;
                        String month=null,year=null,day=null;

                        if(jpostdate!=null)
                        {
                            String[] postDateArray=jpostdate.split("/");

                            month=postDateArray[0];
                            day=postDateArray[1];
                            year=postDateArray[2];
                            if(month.length()==1)
                                month = 0 + month;

                            if(day.length()==1)
                                day = 0 + day;

                            parsed = format.parse(month+"-"+day+"-"+year);
                            sqldate1 = new java.sql.Date(parsed.getTime());

                        }

                    }
                    catch(Exception e)
                    {
                        System.out.println("dates exception found");
                        e.printStackTrace();
                    }

                    System.out.println("------------------------------------------------------------------------------------------------------------");
//                    System.out.println( "found job description as :"+jd);
//                    System.out.println("found job description teaser as :"+jdteaser);
                    System.out.println("found job location as: "+jloc);
                    System.out.println("found job pay as: "+jpay);
                    System.out.println("found job type as: "+jtype);
                    System.out.println("found job industry as: "+jindustry);
                    System.out.println("foudn post date as: "+jpostdate);
                    System.out.println("found sql date as: "+sqldate1.toString());
                    System.out.println("found title as: "+jtitle);
                    System.out.println("found url as a: "+jurl);
                    System.out.println("found zip code as: "+jzipcode);
                    System.out.println("found city and state as :"+city +" "+state);
                    System.out.println("found job id as :"+jid);
                    System.out.println("found email id as: "+jemail);
                    System.out.println("------------------------------------------------------------------------------------------------------------");

                    try
                    {
                        if(jurl!=null)
                        {
                            String language_type="en";
                            //String ref_num="INFOA000A";
                            String ref_num=pubrefnum;
                            //String job_seq_no="INFOA000A"+did;
                            String job_seq_no=pubrefnum+did;
                            String curDate = getCurrentTime();
                            //jtitle=hm.get(ur);
                           // jid=job_seq_no;
                            storeData(did,jtitle,jurl,jid,jd,jdteaser,jcategory,jloc,state,city,jtype,"Snelling",curDate,language_type,ref_num,job_seq_no,sqldate1,jdepartment,jindustry,jzipcode,jpay,jemail);
                            System.out.println("Job saved with seqNO:"+job_seq_no);
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println("Storing Exception found: "+e.getMessage());
                        e.printStackTrace();
                    }

                    //System.exit(0);

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        driver.quit();

        //dropdown id:
        //Content_DropZone1_columnDisplay_controlcolumn_0_WidgetHost_0_WidgetHost_widget_0_ddlPageSize_0
    }

    public static void storeData(int j,String jtitle,String jUrl,String jid,String wholejobcontent,String wholejobcontent1,String category,String loc,String state,String city,String jtype,String company_name,String curDate,String language_type,String ref_num,String job_seq_no,java.sql.Date postdate,String department,String jindustry,String jzipcode,String jpay,String jemail) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException
    {
        PreparedStatement prep=null;
        //Connection con = getConnection();

        try
        {


            // System.out.println("cms id formed as: "+cms);
            prep = con.prepareStatement( "INSERT into jobpulls.snellingjobs(category,city,company_name,date,job_description,job_id,job_location,job_seq_no,job_title,job_url,language_type,ref_num,state,id,job_type,post_date,job_description_teaser,department,industry,zip_code,reserved1,mail_to) values (?, ?,?,?, ?,?, ?,?, ?,?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");


            prep.setString(1,category);
            prep.setString(2,city);
            prep.setString(3,company_name);
            prep.setString(4,curDate);
            prep.setString(5,wholejobcontent);
            prep.setString(6,jid);
            prep.setString(7,loc);
            prep.setString(8,job_seq_no);
            prep.setString(9,jtitle);
            prep.setString(10,jUrl);
            prep.setString(11,language_type);
            prep.setString(12,ref_num);
            prep.setString(13,state);
            prep.setInt(14,j);
            prep.setString(15,jtype);
//			 prep.setDate(16, null);
            prep.setDate(16, postdate);
            prep.setString(17,wholejobcontent1);
            prep.setString(18,department);
            prep.setString(19,jindustry);
            prep.setString(20,jzipcode);
            prep.setString(21,jpay);
            prep.setString(22,jemail);

            prep.executeUpdate();
            //System.out.println("job stored into CC:"+j);
            //System.out.println("JD 3");
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println("JD 2");


    }

    public static void main(String[] args) throws MalformedURLException, InterruptedException
    {
        pubrefnum="SNELA002O";
        //snellingjobs
        driver =new RemoteWebDriver(new URL("http://localhost:9515"), DesiredCapabilities.chrome());
         /*Properties prop = null;
      try {
          prop = new Properties();
          prop.load(new FileInputStream("/home/ubuntu/JobPull/informatica_new/config/robot.properties"));
          //	prop.load(new FileInputStream("/home/manjunath/apply_robots/QvcJobPullRobot/config/robot.properties"));

          refnum = prop.getProperty("refnum");
          pubrefnum = prop.getProperty("refnum");

      } catch (FileNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      }*/

        try
        {

            con=getConnection();
            Statement st = (Statement) con.createStatement();

            st.executeUpdate("DELETE FROM jobpulls.snellingjobs WHERE id>0");
            System.out.println("Database cleared:");
            System.out.println("Inserting new jobs");

            snellingJobs();

            if(con!=null)
                con.close();
            if(driver!=null)
                driver.quit();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }
}
